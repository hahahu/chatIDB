angular.module('socket.service', [])
//用 Factory 就是创建一个对象，为它添加属性，然后把这个对象返回出来。
// 你把 service 传进 controller 之后，在 controller 里这个对象里的属性就可以通过 factory 使用了
.factory('socket', function ($rootScope) {
    //by wzh 2015117 client是在这里 connect 到 server的 ？
    //都不需要 this.socket = io.connect('ws://127.0.0.1:3000');
    var socket = io(); //默认连接部署网站的服务器
    //by wzh 20151201 加上具体 url var socket = io.connect('ws://127.0.0.1:3002');
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {   //手动执行脏检查
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                //console.log("33333333333");
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
})
