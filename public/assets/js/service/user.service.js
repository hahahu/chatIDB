angular.module('user.service', [])
    .factory('userService', function ($rootScope, localStorageService) {
        return {
            get: function (users, nickname, userName) {
                if (users instanceof Array) {
                    for (var i = 0; i < users.length; i++) {
                        //主要使用 userName 判断
                        if (users[i].nickname === nickname || users[i].userName === userName) {
                            return users[i];
                        }
                    }
                } else {
                    return null;
                }
            },
            getID: function (users, ID) {
                if (users instanceof Array) {
                    for (var i = 0; i < users.length; i++) {
                        if (users[i].ID === ID) {
                            return users[i];
                        }
                    }
                } else {
                    return null;
                }
            },
            getFriends: function (userName) {
                console.log("getFriends userName 111 ==" + userName);
                var friendsItem = "friends_" + userName;
                var s = localStorageService.get(friendsItem);
                var x = angular.fromJson(s);
                var friends = [];
                if (x instanceof Array) {
                    for (var i = 0; i < x.length; i++) {
                        friends.push(x[i]);
                    }
                }
                console.log(friends);
                return friends;
            },
            setFriends: function (userName,friends) {
                console.log("setFriends userName 222 ==" + userName);
                console.log(friends);
                var friendsItem = "friends_" + userName;
                localStorageService.set(friendsItem,friends);
                //return friends;
            }

        };
    })

