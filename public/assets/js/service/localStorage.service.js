angular.module('localStorage.service', [])
.factory('localStorageService', [function () {
    return {
        //存储单个 key
        set: function localStorageServiceSet(key, value) {
            if (value) {
                localStorage.setItem(key, angular.toJson(value));
            }
        },
        //读取单个 key
        get: function localStorageServiceGet(key, defaultValue) {
            //console.log('读取单个 key');
            var stored = localStorage.getItem(key);
            try {
                stored = angular.fromJson(stored);
            } catch (error) {
                stored = null;
            }
            if (defaultValue && stored === null) {
                stored = defaultValue;
            }
            console.log(stored);
            return stored;
        },
        //更新某个 key
        update: function localStorageServiceUpdate(key, value) {
            if (value) {
                localStorage.setItem(key, angular.toJson(value));
            }
        },
        //删除单个 key
        remove: function localStorageServiceRemove(key) {
            localStorage.removeItem(key);
        },
        //清除LS,就都没了！谨慎使用
        clear: function localStorageServiceClear() {
            localStorage.clear();
        }
    };
}]);
