angular.module('user.directive', [])
.directive('user', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        templateUrl: 'user.html',
        scope: {
            info: "=",
            iscurrentreceiver: "=",
            setreceiver: "&"
        },
        link: function (scope, elem, attrs, chatCtrl) {
            $timeout(function () {
                elem.find('.avatar').css('background', scope.info.color);
            });
        }
    };
}])
