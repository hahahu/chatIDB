//注册我自己定义的指令 ： 指令注册的方式与 controller 一样,它返回的是一个拥有指令配置属性的简单对象
//第一个参数是这个指令的名字
//第二个参数是一个返回指令定义对象的函数
//如果你的指令依赖于其他的对象或者服务，比如 $rootScope, $http, 或者$compile，他们可以在这个时间被注入
//restrict:这个属性用来指定指令在HTML中如何使用
//template: 这个属性规定了指令被Angular编译和链接（link）后生成的HTML标记
//replace: 这个属性指明生成的HTML内容是否会替换掉定义此指令的HTML元素
//四种形式：E:元素 A:属性 C:CLASS M:注释
angular.module('message.directive', [])
.directive('message', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        templateUrl: 'message.html',
        //为指令生成了隔离作用域
        //@，它将本地作用域和DOM中的属性值绑定起来
        //=与@的不同点在于，@是针对字符串而用，但=是针对某个对象的引用，
        //&符号含义是：对父级作用域进行绑定，并将其中的属性包装成一个函数
        scope: {
            info: "=",
            self: "=",
            scrolltothis: "&"
        },
        //如果要开放出一个API给其他指令用就写在controller中，否则写在link中
        //可以简单理解为，当directive被angular 编译后，执行该方法
        //link函数主要用来操作DOM的
        //link中第一个参数scope基本上就是那个scope
        //elem 就是$('XXXX'),关联的那个DOM元素
        //attrs 就是关联的那个DOM元素的属性，例如{ type: 'modal',animation: 'fade',self="nickname" ...

        link: function (scope, elem, attrs) {
            scope.time = new Date();
            //console.log("info ===");
            //console.log(scope.info);
            $timeout(scope.scrolltothis);
            $timeout(function () {
                elem.find('.avatar').css('background', scope.info.color);
            });
        }
    };
}])
