var myDB123 = {
    name: 'chat0602',
    version: 1,
    db: null,

    createDate: "20160530",
    lastModifed: "20160530",
    author: "wzh",
    description: "本地存储聊天信息\朋友信息等",
    empty: true,

    messagesStore: "messages",//实际会加上登录用户的 userID
    usersStore: "users",
    friendsStore: "friends",
};

angular
    .module("chatRoom", [
        'chatRoom.ctrl',
        'message.directive',
        'user.directive',
        'socket.service',
        'randomColor.service',
        'localStorage.service',
        'user.service',
        'IDB.service',
        'angular-popups'
    ])
    //.constant('useIDB', false)
    .value('useIDB', true)
    //.value('useIDB', false)

    //add by wzh 20160602 配置一下 Popup的参数
    .config(configPopupProvider)

    .run(init);

function configPopupProvider(PopupProvider) {
    PopupProvider.title = '提示';
    PopupProvider.okValue = '确定';
    PopupProvider.cancelValue = '取消';
};

function init($rootScope) {
    //初始化在这里
    console.log("初始化在这里 >>>>>> ");
    //var db = null;
    if (!window.indexedDB) {
        window.indexedDB = window.mozIndexedDB || window.webkitIndexedDB;
    }

    if (!window.indexedDB) {
        console.log("不支持的 IDB ! <<<<<<");
        //使用 LS 完成
    }
    else {
        console.log("IDB 是支持的 ! >>>>>>");
        //初始化数据库
    }
};





