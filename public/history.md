退出时,close

## 20160602
  1 安装一个 ng 的 popup,
    npm install angular-popups
    [参考](http://www.open-open.com/lib/view/open1450624163727.html)
   
  2 npm install到本项目的 node_modules了,怎么让他在 public 的 asset 里面?
    不会啊,直接 copy 过来了
  3 记得app.js里面引入,在 chat.ctrl.js 里面引入 Popup
  4 在 app.js 配置一下 Popup的PopupProvider参数  
  5 断开后弹出提示,接下来在输入框 disable
  6 初始化的receiver 设置为 群聊的 user ,否则,必须点接收者才能显示出来
  7 增加 myPlaceHoder,disconnected,用于控制 form ,在网络断开的时候,不让继续subit
  
## 20160602
  1 index.html 加上一个调试用的删除indexDB垃圾的 button,用完注释掉
  2 以后数据库名称不能随意改变,如有变化,写 upgrade 的脚本,直接删除旧的内容最简单
  3 md 文件中 url连接要显示成链接的话,缩进不能是 tab,最多2个空格

## 20160601
  1 加上socket 其他事件,不是必须,方便测试程序
    [参考](http://www.cnblogs.com/xiezhengcai/p/3956401.html)
  
## 20160531
    1 需要联合索引 from和to,找不到indexDB的api
    2 mongo中有虚拟字段,这indexDB有吗?
    3 先加一个实际字段 friend:from和to的综合
        发给我的信息,friend = from
        我发出的信息 friend = to
        群聊的信息 friend = 'all'
    4 看来换数据库的时候,结构也要有所变化.难怪人家那个wechatDemo的数据结构是那个样子的
    5 总算折腾出来多字段索引,可惜!结果发现也不能一次查询完毕
        多字段索引如下
        var messagesFriendIndex = myDB.messages.createIndex("friendIndex", ['from', 'to'],{unique: false});
        使用方式如下
        var boundKeyRange = IDBKeyRange.only(['wzh','wzh']);
    6 IDBKeyRange 有没有and .or?留待以后继续吧,先使用单字段 friend 进行


## 20160530
    1 客户端使用IDB,上次还没有完成呢
    2 所有的聊天的 get 都到了 publicMessages里面了,都算群聊啊?
    3 给 messages 的存储对象(ObjectStore)创建 2 个索引
    4 分成2个index,查询出来的结果怎么排序?
    
## 20160530

    1 服务端加上 useMongoDB 控制,挑食可以不必开着 mongo
    2 客户端准备改造手机端微信模式
    3 每个朋友的聊天都保存一个 lastMessage
    4 lastMessage 只在客户端保存,服务端不保存,这样保证 insert message的时候不会还要update users
    5 客户端的message数据结构需要改变?目前是这样的
        color:"#36347a"
        content:"22"
        from:"wzh"
        fromID:"13501062476"
        timeStamp:"15:05:17 GMT+0800 (CST)"
        to:"zxc"
        toID:"13522388251"
        type:"normal"
    6 嵌入式文档?可以解决message部分,那么,lastMessage字段怎么办?
    7 不要一次操作2个collection,怎么办?
    8 lastMessage直接查询算了,不改变数据结构!
    9 怎么查询 from或者to某个user的消息的最后一个消息?
    
## 20160515
    1 准备加上 lastMessage
    2 发送消息,西药加一个返回结果状态
    3 服务端断开,怎么捕获?
    4 服务端:需要一份在线状态,不在线的就不要转发了,以免错误
    5 friends 上带着 lastmessage,不和users完全一样?还是users也加上lastmessage?
    6 某个用户是否存在于列表,有什么好办法?除了循环
    7 在线聊天改的过多了,friends属于不一定在线的
    8 
## 20160514
    1 优化代码
    2 friends 和 allusers合并的话,logout下去一个,会remove掉,所以合并不适合
    3 单出来一个列表 friends,类似于qq,这需要修改视图?
    4 remove 掉的是后面那个,但是给不在线的发消息,服务端错误!这个待服务端处理
    5 加朋友不用加 all
    
    
## 20160513
    1 messages 主要使用 userName 判断检索
    2 保存 LS 也该用 userName ,不适用 ID 对应
    3 要保留未读状态,怎么办？
    4 重复登陆问题,服务端??也改成 userName 检索并判断
    5 服务端 :logout 问题:
    6 收到和发送消息,都保存一份 friends
## 20160512
    1 模块分开成独立的目录和js文件
    2 index.html 中 ng-click="setReceiver(user.nickname) 参数改为 user对象,不仅需要一个昵称
    3 登陆\离开的allusers有问题
    4 发一条消息怎么就成了退出?
    
## 20160511
    1 已经有过聊天记录的用户应该列举出来
    2 users列表 前半部分列举有聊天记录的
    3 users 要有记录在线与否,是否有新消息
    4 建立一个 friends 存储对象,白哦是有记录的
    5 所有users 是否需要保存?应该不需要
    6 每次有聊天消息时,update friend objectStore,第一次的话,会自动增加
    7 既要保存消息,又要保存friends,那么,需要有个次序,都要写成 promise,顺序调用
    8 如果分别执行,会是怎样？
    
## 20160510 IDB 处理
    1 一个app初始化一个数据库,第二次就不走upgrade了,后续的 transaction 都是 null
    2 还是从 存储对象来区分,每个登录用户的俩天记录都在 messages + 自己的ID,这样子也不会串门
    3 错了!
    4 数据库open ,upgrade 都是异步的,必须等待反悔成功之后,进行get,add 等操作,否则transaction 都是 null
    5 IDBservice的init,get都已经改成promise,其他待进一步处理
    6 users 列表问题:nickname和userName 有些混乱,怎么统一起来?
    
## 20160510 
    1 客户端登陆发送的请求只包括 userName和pwd
    2 服务端返回详细信息,客户端接收并保存
    3 user存储对象的init数据先不做,待修改为接收到服务端等罗杰过后,再保存
    4 messages存储对象是不是应该带上 ID,以便区分是谁的聊天记录,一个人一个存储对象
    5 数据库也带上我自己的 ID,这样的话,必须先登陆
    6 从数据库名字分开,每个聊天对象的存储都放在同一个 messages里面,后续通过index查询来区分谁和谁
    
## 20160509 IDB 替换 LS
    1 app 主入口 增加一个 .run
    2 数据库初始化在 run() 里面完成
    3 增加2个数据集合 objectStore (相当于表) users,messages
    4 写入数据库不再需要 LS那样,整体 update了,单独追加 最后一条即可
    5 数据库操作全部是异步操作,自定义一个 promise 处理
     
## 20160429
    1 没有点进去过的消息,在此进来就没有了?连同之前的也没了
    2 已读 未读 红点也没有了？
    3 ID,toID,fromID等ID相关字段全部改成 string 方式
    4 私聊的 LS 还不能光是一方 ID 判断,否则,串门了!
        ** 每个人保存的 LS item 规则都是 : "message_"+$scope.ID + "_with_" + 对方
        ** 无论对方时发送方,还是接收方
        ** 如果是数据库,其实就是 发送方 + 接收方 联合查询
    5 公聊不存在该问题,因为都是可见的! LS item 规则都是  "message_"+"all" + "_with_" + 群聊ID(先设置为 0 );
    
## 20150428 
    1 按照data,先假设用户ID,wzh =1 ,cjq =2,zxc =3 mlj=4 lwz=5
    2 pc短先完成 message部分,忽略 lastMessage
    3 每次聊天,保存 LS message + ID
    4 群聊 ID = 0
    5 单挑信息结构
    目前 messages	{"text":"4444","type":"normal","color":"#dd44af","from":"ssss","to":""}
    目标      "isFromeMe": true,
          "content": "你好, 你是谁?",
          "time": "2015-11-22 08:51:02"
    6 收到的消息首先区分群聊还是个人,群聊保存在 ID = 0 的记录里面,无论是谁发出的
    7 为了实现消息 LS 追加,必须首先读出来 LS

## 20160426 改造聊天室 计划
    服务端
        1 服务端加上一个用户列表,只有列表中的用户可以登录
        2 服务端保存聊天记录,待处理
        3 用户注册功能待处理
        4 过客功能待处理

    客户端
        1 增加聊天记录保存LS
        2 LS改造成DB,待处理
        
## 20160426 改造聊天室 
    1 先不改动原有目录结构,asset代表客户端
    2 data目录客户端和服务端分别建立目录
    3 js 目录分别建立 controller,service,directive目录
    4 先完成聊天记录保存
    5 下次登录进来先显示历史记录
    