##20180624
    1 修改 mongo 相关 api ,自定义 promise
        findOne,find,query等 本身可以直接作为 promise返回,但是会和mongoose的promise 有冲突
        提示:Mongoose: mpromise (mongoose's default promise library) is deprecated, plug in your own promise library instead: http://mongoosejs.com/docs/promises.html
    2 也可以直接引入 mongoose 的 promise 参见
        https://github.com/davideicardi/mongoose-promised
    3 自定义 promise 需要 引入 q 模块
        npm install q
        
##20180621
    1 客户端是支持 emoji的,保存到服务端 mongo,使用 rock 看见的是乱码
    2 怎么知道 mongo 是否正确保存了？只有从 mongo 独取出来,正确显示才行啊
    3 怎么做?做一个简单后台?
    
##20160527
    1 从今天开始,copy备份方式反过来,copy出去的不作为最新版,项目名一直维持叫ChatIDB,带日期的都是备份
    2 服务端只需要insert进去所有的messages即可,查询应该是另外一个功能:后台管理
    3 那么,这样子是不是聊天部分就算完成了?
    4 还应该有一个log,记录login\logout?
    5 Model的第一个参数实际是建立的 集合名字,例如:以下建立了一个叫做users的collection,mongo自动转成了小写?
        var User = mongoose.model('Users', schemaUser);
        
##20160526
    1 讲 init 放在 once ,保证本次 app 过程中,只会运行一次
    2 退出 app,下次在运行,还算第一次,所以,还是需要自己控制,不尽兴二次初始化,怎么办?
        待处理吧!先通过手工方法控制
    3 users 就是用初始化好的这个集合吧,query后,返回给客户端
    4 查询单个用户 queryUser,反悔一个 Promise,感觉和 NG 的 Promise不完全一样
    5 这里使用了 Mongoose API 的Promise ,省的自己再去写NG的Promise接口
    6 用户是否已经登陆,重复登陆,待处理!应该写回数据库吗？close或者disConnect的时候, 怎么办?
       这个重连问题复杂,待处理
    7 是否区分 用户名错误或者已经登陆,以后处理!
    8 待增加数据库 Message 集合
    
##20160525
    1 总算明白一点儿 Schema,相当于java里面定义了一个class,包括了数据结构和一些方法
        方法的实现都需要实际通过Model来完成
    2 暴露出去的是公用方法,外部通过require 该js模块之后得到的对象就是一个class,
    3 该 class 就拥有了 export 出去的方法
    4 做几条初始化数据,放在一个对象数组里面,测试时可以直接循环全部插入,实际应该是后台app输入或注册
    
##20160524 学习 mongoose
    1 参考 mongoose 文档
        []:  http://www.nodeclass.com/api/mongoose.html
    2 继续理解 Schema Model Entity
    Model:定义了一个集合的数据结构(相当于一个表的字段)
    Entity:实际插入了一个文档(相当于insert了一条记录)
    
##20160523 mongoDB 服务端
    1 monggo操作有关的部分放在 database/mongo.js
    2 使用 mongoose的API
    3 增加 mongoose 和mongo依赖,在package.json中增加
        "mongodb": "*",
        "mongoose":"*"
    4 如果修改了 mongo.js,必须先 npm install
    5 遇到 ELIFECYCLE 错误
        换个 listen端口,ok
        退出app,再进来,ok
        什么地方问题?和 mongo 有关?
        我一会儿到公司,关于你昨天的问题,清闲看看一下9条
        1 金融有关的请求url请使用post方式
        2 http://jbr.dhbm.cn/opacOpener/getOpenUrlByBookId/-l AND 1122=(SELECT 
        UPPER(XMLType(CHR(60)++CHR(58)....WHEN(1122=1122) THEN ...
        3 这个 getOpenUrlByBookId 谁写的?
        4 后面部分明显是sql注入吧？不是我们程序写的吧？
        5 这个getOpenUrlByBookId是在客户端js写的?还是服务端php写的?
        6 如果php写的,是否使用了TP框架?
        7 如果使用了TP,为什么还有sql注入?
        8 关于第一条建议,如果来不及该用post,先解决sql注入,再解决post
        9 解决sql注入除了依赖TP框架,自身程序也必须有预处理,直接抛弃或返回错误,不要不管不顾交给操作系统