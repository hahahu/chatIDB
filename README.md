AngularJS Chat Room
=====================
一个基于AngularJS、NodeJS、Express、socket.io搭建的在线聊天室。

##DEMO
地址：http://45.79.97.45:3002/

##How to use

  git clone https://github.com/sheila1227/ChatRoom-AngularJS
  
  cd ChatRoom-AngularJS
  
  ...

##本demo下载时已经带着以下3个，如果没有就自己安装
  ### 安装 node模块
    npm install
  ### 安装 express和socket
    npm install --save express
    npm install --save socket.io
  ...
### 运行方式
  node app.js
### 或者
  npm start
### 查看（客户端）
  http://127.0.0.1:3002
### 20160523 启动 mongoDB 服务端
  sudo mongod
  