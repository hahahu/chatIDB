var express = require('express');
var app = require('express')();
var http = require('http').createServer(app);//原生Http服务
var io = require('socket.io')(http);//Socket.io服务


//var user = require('./database/mongo');
var useMongoDB = true;//如果没有数据库,就是用testData

if (useMongoDB)
    var dbChat = require('./database/dbChat');


//几条初始化数据,放在一个对象数组里面,测试时可以直接循环全部插入,
var testData = [
    {
        userID: "13501062476",
        userName: 'wzh',
        nickname: '汪志洪'
    },
    {
        userID: "13901058301",
        userName: 'cjq',
        nickname: '陈军前'
    },
    {
        userID: "13439475146",
        userName: 'mlj',
        nickname: '小马'
    },
    {
        userID: "13522388251",
        userName: 'zxc',
        nickname: '祥子'
    },
];

//express框架：app.use 不是来处理请求的, 而是来加载处理请求的路由模块的参数
//一个请求过来, 被 .use 写好的各个函数依次处理
//Express 内置的 express.static 可以方便地托管静态文件
//这里是指定路由到本地目录下的 public目录，如果客户端不在本地怎么办？
//那就路由到客户端的虚拟目录
//要么，独立写客户端，直接去掉下面这句话
app.use(express.static(__dirname + '/public'));

//add by wzh 20160622 加一个简单的后台
app.use('/admin', express.static(__dirname + '/admin'));

//express框架：app.get 等等 HTTP 请求的操作, 统一在 app.routes 着一个模块里边
app.get('/', function (req, res) {
    //res.sendfile('index.html');
    //by wzh 20151201 简化试试
    res.send('<h1>Welcome Realtime Server 20151204</h1>');
});

//直接渲染
app.get('/admin123', function (req, res) {
    //res.sendfile('index.html');
    //by wzh 20151201 简化试试
    res.send('<h1>Welcome Realtime Server 20160622</h1>');
});

//如果这个use放在get / 后面，将不会直接路由到public下的index,html
//http://127.0.0.1:3002/index.html才会到达
// app.use(express.static(__dirname + '/public'));


var connectedSockets = {};
var allUsers = [{userID: 0, userName: "all", nickname: "所有人", color: "#000"}];//初始值即包含"群聊",用""表示nickname
io.on('connection', function (socket) {

    console.log("connection -who ");
    socket.on('addUser', function (data) { //有新用户进入聊天室
        console.log('addUser---+data=');
        console.log(data);

        if (!useMongoDB) {
            var positonFound = -1;
            for (i in testData) {
                if (data.userName == testData[i].userName) {
                    positonFound = i;
                    break;
                }
            }
            if (positonFound < 0) {
                //不在testData中
                console.log('不存在--' + data.userName);
                socket.emit('userAddingResult', {result: false});
                return;
            }
            if (connectedSockets[data.userName]) {
                //昵称已被占用，直接返回 false
                console.log('昵称已被占用，直接返回 false--' + this.data.userName);

                socket.emit('userAddingResult', {result: false});
            }
            else {
                this.data = testData[positonFound];
                this.data.hasLogined = true;
                var resultOK = {};
                resultOK.result = true;
                resultOK.data = this.data;

                socket.emit('userAddingResult', resultOK);

                //同时广播给所有人,XXX登陆了
                socket.userID = this.data.userID;
                socket.userName = this.data.userName;
                socket.nickname = this.data.nickname;

                //console.log('分配socket.nickname');
                // console.log(socket.userName);
                connectedSockets[socket.userName] = socket;//保存每个socket实例,发私信需要用

                //加入到 聊天用户列表
                allUsers.push(this.data);
                console.log('allUser');
                console.log(allUsers);
                socket.broadcast.emit('userAdded', this.data);//广播欢迎新用户,除新用户外都可看到
                socket.emit('allUser', allUsers);//将所有在线用户发给新用户，让他们也都看到最新列表

            }
            //如果不使用 mongoDB,在这里直接返回,后面的是mongo处理
            return;
        }
        else {
            //如果使用 mongoDB,才会执行下面的部分,实际的时候可以直接删除前面部分
            //查询数据库的 users,异步查询的dbChat返回的是一个Promise
            dbChat.queryUser(data)
                //找到了
                .then(function (result) {
                        this.data = result
                        //console.log('333333444');
                        //console.log(this.data);
                        // console.log('33355555');
                        //console.log(connectedSockets);

                        //设置他的状态为:已登录,待协会数据库
                        this.data.hasLogined = true;
                        //不允许重复登陆
                        if (connectedSockets[this.data.userName]) {
                            //昵称已被占用，直接返回 false
                            console.log('昵称已被占用，直接返回 false--' + this.data.userName);

                            socket.emit('userAddingResult', {result: false});
                        }
                        else {
                            //这是登录正确了!
                            var resultOK = {};
                            resultOK.result = true;
                            resultOK.data = this.data;

                            this.data.result = true;
                            socket.emit('userAddingResult', resultOK);

                            //同时广播给所有人,XXX登陆了
                            socket.userID = this.data.userID;
                            socket.userName = this.data.userName;
                            socket.nickname = this.data.nickname;

                            //console.log('分配socket.nickname');
                            // console.log(socket.userName);
                            connectedSockets[socket.userName] = socket;//保存每个socket实例,发私信需要用

                            //加入到 聊天用户列表
                            allUsers.push(this.data);
                            console.log('allUser');
                            console.log(allUsers);
                            socket.broadcast.emit('userAdded', this.data);//广播欢迎新用户,除新用户外都可看到
                            socket.emit('allUser', allUsers);//将所有在线用户发给新用户，让他们也都看到最新列表

                            //活动记录保存到数据库
                            var mLog = {
                                type: 'welcome',
                                userID: socket.userID,
                                userName: socket.userName
                            };
                            var insertLogs = dbChat.insertLog(mLog);
                        }
                    }
                )
                //错了,或者没有找到
                .then(null, function (err) {
                    console.log('444444555');
                    console.log(err);
                    //这是登陆错误
                    // console.log('不存在--' + this.data.userName);
                    socket.emit('userAddingResult', {result: false});
                });
        }


    });

    //服务端接收到了某个人（发送方）向另一人(接收方)发送消息的请求
    //没有给 发送方 返回一个 状态报告？是不是socket.js里面处理了？
    socket.on('addMessage', function (data) { //有用户发送新消息
        //消息体数据结构定义 var msg={
        // text:$scope.words,
        // type:"normal",
        // color:$scope.color,
        // from:$scope.nickname,
        // to:$scope.receiver};

        console.log('addMessage---');
        //console.log(data);

        //群聊可能没有带着 toID和to,不能空着走下去,给他补上
        if (typeof (data.toID) == 'undefined' || data.toID == null || typeof (data.to) == 'undefined' || data.to == null) {
            //直接设置为 群聊 userID: 0, userName: "all", nickname: "所有人"
            data.toID = '0';
            data.to = 'all';
        }

        //保存到数据库,不区分私聊和群聊
        if (useMongoDB) {
            var insertMessages = dbChat.insertMessages(data);
        }


        //服务端仅仅解析了data(客户端叫msg)的 to (消息接收方)，直接转发
        if (data.to != 'all') {//发给特定用户
            //console.log( data.to);
            console.log('to' + data.to);
            connectedSockets[data.to].emit('messageAdded', data);
        } else {//群发
            console.log('to -all');
            socket.broadcast.emit('messageAdded', data);//广播消息,除原发送者外都可看到
        }


    });


    socket.on('disconnect', function () {  //有用户退出聊天室
            console.log('下去啦 disconnect---' + socket.nickname);

            socket.broadcast.emit('userRemoved', {  //广播有用户退出
                userName: socket.userName,
                nickname: socket.nickname
            });

            //从 allUsers 列表 删除该user
            for (var i = 0; i < allUsers.length; i++) {
                if (allUsers[i].userName == socket.userName) {
                    allUsers.splice(i, 1);
                }
            }
            //同时要 close（这里叫 delete）掉该user的socket
            delete connectedSockets[socket.userName]; //删除对应的socket实例

            //活动记录保存到数据库
            var mLog = {
                type: 'bye',
                userID: socket.userID,
                userName: socket.userName
            };

            if (useMongoDB) {
                var insertLogs = dbChat.insertLog(mLog)
            }


        }
    );
});

http.listen(3002, function () {
    console.log('listening on *:3002===');
});
